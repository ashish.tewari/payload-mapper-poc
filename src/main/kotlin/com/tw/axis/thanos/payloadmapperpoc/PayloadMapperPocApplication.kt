package com.tw.axis.thanos.payloadmapperpoc

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class PayloadMapperPocApplication

fun main(args: Array<String>) {
	runApplication<PayloadMapperPocApplication>(*args)
}
